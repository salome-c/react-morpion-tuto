import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

  // Fonction composant qui retourne un bouton = ne gère pas d'état local
  // onClick est un écouteur pour l'élément natif button, cet attribut a un sens particulier pour React
  // le bouton affiche la valeur reçue en paramètre
  function Square(props) {
    return (
      <button className="square" onClick={props.onClick}>
        {props.value}
      </button>
    );
  }
  
  class Board extends React.Component {
    // Retourne un nouveau Square en lui passant les props, i étant l'indice du Square
    renderSquare(i) {
      return (
        <Square 
          value={this.props.squares[i]} 
          onClick={() => this.props.onClick(i)}
        />
      );
    }
    // Retourne le Board (9 Square avec indices de 0 à 8)
    render() {
      return (
        <div>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
      );
    }
  }
  
  class Game extends React.Component {
    // Ctor permet de gérer l'état initial local du Game
    constructor(props) {
      super(props);
      this.state = {
        history: [{
          // Tableau de 9 éléments null
          squares: Array(9).fill(null),
        }],
        stepNumber: 0,
        // Booléen qui bascule quand un joueur intervient (commence par X)
        xIsNext: true,
      };
    }
    // Méthode appelée au clic sur un Square (bouton carré)
    handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      // Copie du tableau squares stocké dans le state
      const squares = current.squares.slice();
      // Court-circuitage si un joueur a déjà gagné ou si le Square cliqué est déjà rempli
      if (calculateWinner(squares) || squares[i]) {
        return;
      }
      // Le Square cliqué affiche X si xIsNext = true, sinon O
      squares[i] = this.state.xIsNext ? 'X' : 'O';
      // Mise à jour de l'état local (historique + tableau de squares + basculement du bool xIsNext)
      this.setState({
        // concat ne modifie pas le tableau d'origine contrairement à push
        history: history.concat([{
          squares: squares,
        }]),
        stepNumber: history.length,
        xIsNext: !this.state.xIsNext,
      });
    }
    jumpTo(step) {
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
      });
    }
    render() {
      const history = this.state.history;
      const current = history[this.state.stepNumber];
      // Calcule le gagnant au rafraichissement
      const winner = calculateWinner(current.squares);
      const moves = history.map((step, move) => {
        const desc = move ?
        'Revenir au tour n°' + move :
        'Revenir au début de la partie';
        return (
          <li key={move}>
            <button onClick={() => this.jumpTo(move)}>{desc}</button>
          </li>
        );
      });
      let status;
      // Statut basé sur l'existence ou non d'un gagnant
      if (winner) {
        status = winner + ' a gagné';
      } else {
        status = 'Prochain joueur : ' + (this.state.xIsNext ? 'X' : 'O');
      }
      return (
        <div className="game">
          <div className="game-board">
            <Board 
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  const root = ReactDOM.createRoot(document.getElementById("root"));
  root.render(<Game />);

  function calculateWinner(squares) {
    // 3 lignes, 3 colonnes et 2 diagonales possibles pour gagner
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    // Parcourt les possibilités et les compare avec les placements de X ou O pour retourner le gagnant sinon null
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }
  
  // ========================================

  // Game > Board > Square

  // Les composants enfants peuvent communiquer entre eux en remontant une info au composant parent qui la retransmet aux enfants via les props du state
  // = onClick du Square

  // Immutabilité (spread operator) :
  // permet de conserver l'historique des modifications pour revenir en arrière, détecter les modifications (changement de référence) et rafraîchir un compo
  // let player = {score: 1, name: 'Jérémie'};
  // let newPlayer = {...player, score: 2};

  // Map (tableau) :
  // const numbers = [1, 2, 3];
  // const doubled = numbers.map(x => x * 2); // [2, 4, 6]

  // Key : si la clé d’un composant change, ce composant sera détruit et ré-créé avec un nouvel état